var builder = require('botbuilder');
var dialogHelper = require('../helpers/dialog.helper');

exports.chat = chat;

function chat(connector) {
  let bot = new builder.UniversalBot(connector, [
    (session) => dialogHelper.showGreetMessages(session),
    (session, results) => dialogHelper.displayDialog(session, results, 'issueDialog'),
    (session, results) => dialogHelper.displayDialog(session, results, 'prorityDialog'),
    (session, results) => dialogHelper.displayDialog(session, results, 'checkSettings'),
    (session, results) => dialogHelper.mockDeviceOrHelp(session, results),
    (session, results) => dialogHelper.askSolvedIssue(session, results),
    (session, results) => dialogHelper.helpOrRunDiagnostics(session, results),
    (session, results, next) => dialogHelper.mockRunningDiagnosticsOrCloseMessage(session, results, next),
    (session, results) => dialogHelper.displayDialog(session, results, 'cantFindSolution'),
    (session, results) => dialogHelper.createIssue(session, results)
  ]);

  bot.dialog('cantFindSolution', [
    function (session) {
      builder.Prompts.choice(session, `Can’t seem to find a suitable remediation, do you want me to create an Issue for you?`, ['Yes', 'No'], { listStyle: builder.ListStyle.button });
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]),

  bot.dialog('dontCreateIssue', [
    function (session) {
      session.send('Very sorry Johnny. We continually improve our service. We will notify our support staff of your experience.');
      session.endConversation();
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]),

  bot.dialog('createIssue', [
    function (session) {
      session.send('Issue has created and we will notify you of status, Thank you Johnny');
      session.endConversation();
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]),

  bot.dialog('runDiagnostics', [
    function (session) {
      builder.Prompts.choice(session, 'Do you want me to run diagnostics?', ['Yes', 'No'], { listStyle: builder.ListStyle.button });
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]);

  bot.dialog('solvedIssueQuestion', [
    function (session) {
      builder.Prompts.choice(session, 'Did this solved your problem?', ['Yes', 'No'], { listStyle: builder.ListStyle.button });
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]);


  bot.dialog('help', [
    function (session) {
      builder.Prompts.choice(session, 'Do you want me to help you with anything else?', ['Yes', 'No'], { listStyle: builder.ListStyle.button });
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]);

  bot.dialog('endConversation', [
    function (session) {
      builder.Prompts.choice(session, 'Do you want me to help you with anything else?', ['Yes', 'No'], { listStyle: builder.ListStyle.button });
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]);

  bot.dialog('selectDevice', [
    function (session) {
      builder.Prompts.choice(session, 'I have a problem with my...', ['iPhone 7', 'Macbook Pro', 'iPad Mini'], { listStyle: builder.ListStyle.button });
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]);

  bot.dialog('issueDialog', [
    function (session) {
      builder.Prompts.choice(session, `It's not...`, ['Connecting to wi-fi', 'Powering on'], { listStyle: builder.ListStyle.button });
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]);


  bot.dialog('prorityDialog', [
    function (session) {
      builder.Prompts.choice(session, 'This is..', ['An emergency', 'Somewhat of an issue', 'Not urgent'], { listStyle: builder.ListStyle.button });
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]);

  bot.dialog('checkSettings', [
    function(session) {
      builder.Prompts.choice(session, 'Do you want me to check your settings?', ['Yes', 'No'], { listStyle: builder.ListStyle.button });
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]);

  bot.dialog('mockFixingDevice', [
    function (session) {
      session.send(`Sure I'll check your settings`);
      session.send(`I know what's wrong`);
      session.send(`I'm fixing it`);
      session.send(`I'm checking everything`);
      session.send(`Let me try something else`);
      session.send(`All done`);
      session.endDialog();
    },
    function (session, results) {
      session.endDialogWithResult(results);
    }
  ]);

  bot.on('conversationUpdate', function (message) {
    if (message.membersAdded) {
      message.membersAdded.forEach(function (identity) {
        if (identity.id === message.address.bot.id) {
            bot.beginDialog(message.address, '/');
        }
      });
    }
  });
}

