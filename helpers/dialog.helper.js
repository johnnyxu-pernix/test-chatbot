exports.mockDeviceOrHelp = mockDeviceOrHelp;
exports.askSolvedIssue = askSolvedIssue;
exports.helpOrRunDiagnostics = helpOrRunDiagnostics;
exports.mockRunningDiagnosticsOrCloseMessage = mockRunningDiagnosticsOrCloseMessage;
exports.createIssue = createIssue;
exports.showGreetMessages = showGreetMessages;
exports.displayDialog = displayDialog;

function showGreetMessages (session) {
  session.send('Hi Johnny');
  session.send('What I can help you with?')
  session.beginDialog('selectDevice');
}

function displayDialog (session, results, dialogName) {
  session.beginDialog(dialogName);
}

function mockDeviceOrHelp (session, results) {
  session.dialogData.checkSettings = results.response.entity;
  if (session.dialogData.checkSettings === 'Yes') {
    session.beginDialog('mockFixingDevice');
  } else {
    session.beginDialog('help');
  }
}

function askSolvedIssue (session, results) {
  session.dialogData.needHelp = results.response.entity;

  if (session.dialogData.needHelp === 'No') {
    session.endConversation();
  } else {
    if (session.dialogData.needHelp === 'Yes') {
      session.reset();
    } else {
      session.beginDialog('solvedIssueQuestion');
    }
  }
}

function helpOrRunDiagnostics (session, results) {
  session.dialogData.solvedProblem = results.response.entity;

  if (session.dialogData.solvedProblem === 'Yes') {
    session.beginDialog('help');
  } else {
    session.beginDialog('runDiagnostics');
  }
}

function mockRunningDiagnosticsOrCloseMessage (session, results, next) {
  session.dialogData.runDiagnostics = results.response.entity;

  if (session.dialogData.runDiagnostics === 'Yes') {
    session.send('running diagnostics...');
    next();
  } else {
    session.send('Very sorry Johnny. We continually improve our service. We will notify our support staff of your experience.');
    session.endConversation();
  }
}

function createIssue (session, results) {
  session.dialogData.createIssue = results.response.entity;

  if (session.dialogData.createIssue === 'Yes') {
    session.beginDialog('createIssue');
  } else {
    session.beginDialog('dontCreateIssue');
  }
}